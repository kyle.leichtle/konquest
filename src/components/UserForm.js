import React from 'react'
import PropTypes from 'prop-types';
import Button from 'material-ui/Button';
import Input from 'material-ui/Input';
import Select from 'material-ui/Select';
import { MenuItem } from 'material-ui/Menu';
import Checkbox from 'material-ui/Checkbox';
import { FormLabel, FormGroup } from 'material-ui/Form';
const Todo = (props) => {

    return (

        <FormGroup onSubmit={() =>{ props.submit(props.info)}}>
            <FormLabel htmlFor="firstName">last name
                <Input type="text" name="firstName" ref={node => {
                    props.info.firstName = node
                }}/>
            </FormLabel>

            <FormLabel htmlFor="lastName">last name
                <Input type="text" name="lastName" ref={node => {
                    props.info.lastName = node
                }}/>
            </FormLabel>

            <FormLabel htmlFor="email">email
                <Input type="text" name="email" ref={node => {
                    props.info.email = node
                }}/>
            </FormLabel>

            <FormLabel htmlFor="charClass">class
                <Select name="Class" value="Class" input={<Input id="class" />} ref={node => {
                    props.info.charClass = node
                }}>
                    <MenuItem value="null" defaultValue>[Select your class]</MenuItem>
                    <MenuItem value="8-Bit Hero">8-Bit Hero</MenuItem>
                    <MenuItem value="Android">Android</MenuItem>
                    <MenuItem value="Bard">Bard</MenuItem>
                    <MenuItem value="Catgirl">Catgirl</MenuItem>
                </Select>
            </FormLabel>

            <FormLabel htmlFor="regNum">reg #
                <Input type="number" name="regNum" ref={node => {
                    props.info.regNum = node
                }}/>
            </FormLabel>

            <FormLabel htmlFor="xp">xp
                <Input type="number" name="xp" ref={node => {
                    props.info.xp = node
                }}/>
            </FormLabel>

            <FormLabel htmlFor="feets">feets
                <Input type="number" name="feets" ref={node => {
                    props.info.feets = node
                }}/>
            </FormLabel>

            <FormLabel htmlFor="attendingNextYear">attending next year
                <Checkbox name="attendingNextYear" ref={node => node ? props.info.attendingNextYear = node.checked : false } onChange={node => {
                    props.info.attendingNextYear = node.target.checked
                }}/>
            </FormLabel>


            <Button type="submit">
                Add Todo
            </Button>
        </FormGroup>
    )
}

Todo.propTypes = {
    info: PropTypes.shape({
        firstName: PropTypes.string.isRequired,
        lastName: PropTypes.string.isRequired,
        email: PropTypes.string.isRequired,
        charClass: PropTypes.string.isRequired,
        regNum: PropTypes.number.isRequired,
        attendingNextYear: PropTypes.bool
    })
}

export default Todo

