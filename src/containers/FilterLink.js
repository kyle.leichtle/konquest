import { connect } from 'react-redux'
import { setVisibilityFilter } from '../modules/visibilityFilter.js'
import Link from '../components/Link'
import { compose } from 'react-apollo';

const mapStateToProps = (state, ownProps) => {
    return {
        attendingNextYear: ownProps.filter === state.visibilityFilter
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        onClick: () => {
            dispatch(setVisibilityFilter(ownProps.filter))
        }
    }
};

const FilterLink = compose(
    connect(mapStateToProps, mapDispatchToProps)
)(Link);

export default FilterLink;