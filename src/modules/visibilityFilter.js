//export const INCREMENT_REQUESTED = 'counter/INCREMENT_REQUESTED'
//export const INCREMENT = 'counter/INCREMENT'
//export const DECREMENT_REQUESTED = 'counter/DECREMENT_REQUESTED'
//export const DECREMENT = 'counter/DECREMENT'

const visibilityFilter = (state = 'SHOW_ALL', action = {}) => {
  switch (action.type) {
    case 'SET_VISIBILITY_FILTER':
      return action.filter;
    default:
      return state
  }
};


export default visibilityFilter

export const setVisibilityFilter = (filter) => {
  return {
    type: 'SET_VISIBILITY_FILTER',
    filter
  }
}