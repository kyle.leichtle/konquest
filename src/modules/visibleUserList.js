import { push } from 'react-router-redux'
import axios from 'axios'

const initialState = {
  id: 1
};

export default (state = initialState, action = {}) => {

  switch (action.type) {

    default:
      return state
  }
}
//export function removeUser(dispatch) {
//  return function(dispatch) {
//    return dispatch(push(`/user/2`));
//  }
//}
export function removeUser(id) {
  return (dispatch) => {

    //dispatch({id: id, type: 'REMOVE_USER'});
    return axios.post('/graphql', {
        query: 'mutation{removeUser(id:'+id+'){id}}'
    })
  }
}

export const getUserDetails = (id) => {

  return dispatch => {
    dispatch(push(`/user/${id}`));
    //dispatch({
    //  type: USER_CLICKED,
    //  id: id
    //});
  }
};

