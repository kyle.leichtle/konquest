import React from 'react'
import { render } from 'react-dom'
//import { Provider } from 'react-redux'
import { ConnectedRouter } from 'react-router-redux'
import store, { history } from './store'
import App from './containers/App'
//import {createStore, combineReducers, applyMiddleware, compose} from 'redux';
import { ApolloClient, ApolloProvider, createNetworkInterface } from 'react-apollo';

import 'sanitize.css/sanitize.css'
import './index.css'

const client = new ApolloClient({
    networkInterface: createNetworkInterface({
        uri: 'http://localhost:3000/graphql',
    }),
});


const target = document.querySelector('#root');

render(
  <ApolloProvider store={store} client={client}>
    <ConnectedRouter history={history}>
        <App />
    </ConnectedRouter>
  </ApolloProvider>,
  target
)
