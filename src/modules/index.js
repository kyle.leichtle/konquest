import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import counter from './counter'
import visibilityFilter from './visibilityFilter'
import visibileUserList from './visibleUserList.js'

export default combineReducers({
  router: routerReducer,
  visibilityFilter,
  visibileUserList,
  counter
})
