import React from 'react'
import PropTypes from 'prop-types';
const Link = ({ attendingNextYear, children, onClick }) => {
    if (attendingNextYear) {
        return <span>{children}</span>
    }

    return (
        <a href="#filterAttendance"
           onClick={e => {
               e.preventDefault()
               onClick()
           }}
        >
            {children}
        </a>
    )
}

Link.propTypes = {
    attendingNextYear: PropTypes.bool.isRequired,
    children: PropTypes.node.isRequired,
    onClick: PropTypes.func.isRequired
}

export default Link