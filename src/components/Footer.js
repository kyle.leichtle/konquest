import React from 'react'
import FilterLink from '../containers/FilterLink'

const Footer = () => (
    <p>
        Show:
        {" "}
        <FilterLink filter="SHOW_ALL">
            All
        </FilterLink>
        {", "}
        <FilterLink filter="SHOW_ATTENDING">
            Attending Next Year
        </FilterLink>
        {", "}
        <FilterLink filter="SHOW_NOTATTENDING">
            NOT Attending Next Year
        </FilterLink>
    </p>
)

export default Footer