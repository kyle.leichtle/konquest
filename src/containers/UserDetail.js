import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { graphql, gql, compose } from 'react-apollo';
import UserProfile from '../components/UserProfile.js'


const mapStateToProps = (state, props) => {
    return {}
};

const mapDispatchToProps = dispatch => bindActionCreators({

}, dispatch);

const getUserById = gql`
query getUser($id: Int! ) {
    users(id: $id) {
        id
        firstName
        lastName
        email
        charClass
        regNum
        xp
        feets
    }
}
`;

const UserProfilelWithData = compose(
    graphql(getUserById, {
        options: props => ({
            variables: {
                id: props.match.params.id,
            }
        })
    }),

    connect(mapStateToProps, mapDispatchToProps)
)(UserProfile);


export default UserProfilelWithData;