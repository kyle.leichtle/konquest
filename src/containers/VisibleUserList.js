import { connect } from 'react-redux'
import { getUserDetails } from '../modules/visibleUserList.js'
import UserList from '../components/UserList'
import { graphql, gql, compose } from 'react-apollo';


const getVisibleUsers = (users, filter) => {

    switch (filter) {
        case 'SHOW_ALL':
            return users;
        case 'SHOW_ATTENDING':
            return users.filter(t => t.attendingNextYear);

        case 'SHOW_NOTATTENDING':
            return users.filter(t => !t.attendingNextYear);

        default:
            return users;
    }
};


const mapStateToProps = (state, ownProps) => {

    return {
        users: getVisibleUsers(ownProps.data.users, state.visibilityFilter)
    }

};

const mapDispatchToProps = (dispatch) => {

    return {
        onOpenUserDetail: (id) => {
            console.log("onOpenUserDetail()!")
            dispatch(getUserDetails(id));
        },
        //onRemoveUser: (id) => {
        //  console.log('onRemoveUser()!', id)
        //    dispatch(removeUser(id))
        //}
    }
};

const attendees = gql`
query TodoAppQuery {
    users {
       id
       firstName
       lastName
       email
       charClass
       regNum
       attendingNextYear
     }
}
`;

const changeUserAttendance = gql`
mutation toggleUserAttendance ($id: Int!, $attendingNextYear: Boolean!){
    toggleUserAttendance(id: $id, attendingNextYear: $attendingNextYear) {
        id
    }
}
 `;
const removeUser = gql`
mutation toggleUserAttendance ($id: Int!,){
    removeUser(id: $id) {
        id
    }
}
 `;

const VisibleUserListWithData = compose(
    graphql(attendees),
    graphql(changeUserAttendance, {
        props: ({ mutate, ownProps }) => ({
            toggleUserAttendance: (id, attendingNextYear) => {
                mutate({
                    variables: {
                        id: id,
                        attendingNextYear: attendingNextYear
                    }
                }).then( () => {
                    ownProps.data.refetch();
                })
            }
        })
    }),
    graphql(removeUser, {
        props: ({ mutate, ownProps }) => ({
            onRemoveUser: (id) => {
                mutate({
                    variables: {
                        id: id
                    }
                }).then( () => {
                    ownProps.data.refetch();
                })
            }
        })
    }),
    //graphql(attendees, {
    //    options: props => ({
    //        variables: { users: props.data },
    //    })
    //}),
    // graphql(filterAttendees, {
    //     options: { variables: { attendingNextYear: false } }
    // }),
    // graphql(mutation, mutationOptions),
    connect(mapStateToProps, mapDispatchToProps)
)(UserList);


export default VisibleUserListWithData;